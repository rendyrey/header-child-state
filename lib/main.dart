import 'package:flutter/material.dart';
import 'blocs/count2_bloc.dart';
import 'header.dart' as Header;
import 'child.dart' as Child;
import './blocs/main_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
       
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

 
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // int _counter = 0;

  // void _incrementCounter() {
  //   setState(() {
     
  //     _counter++;
  //   });
  // }

  @override
  void initState() {
    mainBloc =
        MainBloc();
        count2Bloc = Count2Bloc();
    // TODO: implement initState
    super.initState();
    
  }

    @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
        child: new Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
                Tab(text: "Header"),
                Tab(text: "Child"),
            ],
          ),
          title: Text("Header Child"),
        ),
        body: TabBarView(
          children: [
            Header.Counter1(),
            Child.Counter2(),  
          ],
        )
      )
    );
  }
  // @override
  // Widget build(BuildContext context) {
   
  //   return Scaffold(
  //     appBar: AppBar(
       
  //       title: Text(widget.title),
  //     ),
  //     body: Center(
        
  //       child: Column(
          
  //         // horizontal).
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         children: <Widget>[
  //           Text(
  //             'You have pushed the button this many times:',
  //           ),
  //           Text(
  //             '$_counter',
  //             style: Theme.of(context).textTheme.display1,
  //           ),
  //         ],
  //       ),
  //     ),
  //     floatingActionButton: FloatingActionButton(
  //       onPressed: _incrementCounter,
  //       tooltip: 'Increment',
  //       child: Icon(Icons.add),
  //     ), // This trailing comma makes auto-formatting nicer for build methods.
  //   );
  // }
}
